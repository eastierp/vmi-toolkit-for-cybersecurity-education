#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <stdio.h>
#include <time.h>
#include <libvmi/libvmi.h>
#include <libvmi/nopdetect.h>

uint32_t
bitsToInt (const unsigned char *bits, uint32_t offset)
{
  uint32_t result = 0;
  int n;
  for (n = sizeof (result); n >= 0; n--) {
    result = (result << 8) + bits[n + offset];
  }
  return result;
}

unsigned char
read_char (vmi_instance_t * instance, uint32_t virt_address, int pid)
{
  char *memory = NULL;
  uint32_t offset = 0;
  memory = vmi_read_str_va (*instance, virt_address, pid);
  if (memory) {
    unsigned char retval = memory[offset];
    if (memory) {
      free (memory);
      memory = NULL;
    }
    return retval;
  }
  else {
    return 0;
  }
}





uint32_t
read_space (vmi_instance_t * instance, uint32_t virt_address, int pid, int size)
{

  unsigned char *memory = NULL;
  char *data;
  uint32_t val = NULL;
  uint32_t offset = 0;
  int i;
  size_t bytes_read = 0;
  if (size == 6144)
    return 0;
  else
    //printf("\n address  to be read: 0x%x\n",virt_address);
    data = (char *) malloc (size + 1);

   vmi_read_va (*instance, virt_address, pid, size - 1, data, &bytes_read);
//  printf("\nbytes read %d\n", bytes_read );
/*  for( i=0; i < bytes_read-1; i++)
     {
     printf("%x",data[i]);
     } 

  printf("\n");
*/
//  cat_my_hex (data, bytes_read,virt_address);
}



uint32_t
read_int (vmi_instance_t * instance, uint32_t virt_address, int pid)
{
  uint32_t retval;

  vmi_read_32_va (*instance, virt_address, pid, &retval);
  if (retval) {
    return retval;
  }
  else {
    return 0;
  }
}




int
main (int argc, char **argv)
{
  vmi_instance_t vmi;
  int pid = 0;
  uint32_t peb = 0;
  uint32_t dom, temp;
  unsigned i, j;
  unsigned long tasks_offset = 0, pid_offset = 0, name_offset = 0;   

  /* this is the VM that we are looking at */
  char *name = argv[1];
  /* this is the process that we are looking at */
  pid = atoi (argv[2]);

    if (VMI_FAILURE ==
            vmi_init_complete(&vmi, name, VMI_INIT_DOMAINNAME, NULL,
                              VMI_CONFIG_GLOBAL_FILE_ENTRY, NULL, NULL)) {
        printf("Failed to init LibVMI library.\n");
        return 1;
    }

    /* init the offset values */
    if (VMI_OS_LINUX == vmi_get_ostype(vmi)) {
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_tasks", &tasks_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_name", &name_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "linux_pid", &pid_offset) )
            goto error_exit;
    } else if (VMI_OS_WINDOWS == vmi_get_ostype(vmi)) {
        if ( VMI_FAILURE == vmi_get_offset(vmi, "win_tasks", &tasks_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "win_pname", &name_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "win_pid", &pid_offset) )
            goto error_exit;
    } else if (VMI_OS_FREEBSD == vmi_get_ostype(vmi)) {
        tasks_offset = 0;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "freebsd_name", &name_offset) )
            goto error_exit;
        if ( VMI_FAILURE == vmi_get_offset(vmi, "freebsd_pid", &pid_offset) )
            goto error_exit;
    }

  /* pause the vm for consistent memory access */
  //vmi_pause_vm(vmi);

  /*
   * access the memory 
   */
//   printf("2\n");
  for (i = 0x7FFD0000; i <= 0x7FFDF000; i += 0x1000) {
 //    printf("3 %x %d %d\n",i,read_int (&vmi, i + 0xa4, pid),read_int (&vmi, i + 0xa8, pid));  
    if (read_int (&vmi, i + 0xa4, pid) == 6 && read_int (&vmi, i + 0xa8, pid) == 1) {
      printf("Found PEB at %x\n", i);
      peb = i;
      break;
    }
  
  }

  
  if (peb != 0) {

      int tSize = 0,found=0;

      addr_t env, proc_par;	
	char search_str [] = {'V',0x00, 'M', 0x00, 'I', 0x00, 'V', 0x00, 'A', 0x00, 'R', 0x00, 'S', 0x00};

	char mal_str [] = {'M',0x00, 'A', 0x00, 'L', 0x00, 'W', 0x00, 'A', 0x00, 'R', 0x00, 'E', 0x00};

      char buf[sizeof(search_str)];
  	size_t bytes_read = 0;

  //  	buf = (char *) malloc (sizeof(search_str));
//	memset(buf, 0, sizeof(buf));


	// _RTL_USER_PROCESS_PARAMETERS in _PEB
      proc_par = read_int (&vmi, peb + 0x10, pid);
	printf("Process Parameter Pointer: %lx\n", proc_par);

	// ENVIRONMENT in _RTL_USER_PROCESS_PARAMETERS
      env = read_int (&vmi, proc_par + 0x48, pid);
	printf("Environment Pointer: %lx\n", env);

        
	for(i=0; i<5000; i++)
	{

	     vmi_read_va(vmi, env+i, pid, sizeof(buf), buf, &bytes_read );
	     
//vmi_print_hex(buf, sizeof(buf));
	     j=0;
	     while(j<sizeof(search_str)) {
		if(buf[j] == search_str[j]) j++;
		else {j=0; break;}
	     }

	     if (j == sizeof(search_str))
	     {	
		vmi_print_hex(buf, sizeof(buf));
		printf("Found at %x\n",env+i);	
		found=1;
		break;
	     }
	
	}


	if(found == 1) {
	    vmi_write_va(vmi, env+i, pid, sizeof(mal_str)-1, mal_str, &bytes_read );
	    vmi_read_va(vmi, env+i, pid, sizeof(buf), buf, &bytes_read );     
	    vmi_print_hex(buf, sizeof(buf));
	}


  }


error_exit:
  /* resume the vm */
  //vmi_resume_vm(vmi);

  /* cleanup any memory associated with the LibVMI instance */
  vmi_destroy (vmi);


  return 0;
}






